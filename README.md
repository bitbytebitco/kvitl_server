# Description
This is the backend service that supports the Android frontend portion of the "Kvitl" coffee tab system. 
"Kvitl" (which means "note") is a solution that was provided to replace a legacy paper system for a 
client of Bit Byte Bit's. This system keeps track of customer's coffee bar tab amounts. It also 
provides a way for the business operator to enter and update product information, keep track of transactions, 
and keep a register of the customers.    

## Dependencies
```python
backports-abc==0.5
cffi==1.11.5
enum34==1.1.6
futures==3.2.0
idna==2.7
ipaddress==1.0.22
mysql-connector==2.1.6
pyasn1==0.4.4
pycparser==2.19
PyJWT==1.6.4
singledispatch==3.4.0.3
six==1.11.0
tornado==5.1.1
```
