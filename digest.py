import hmac
import hashlib

try:
    from urllib.parse import parse_qs, quote
except ImportError:
    from urlparse import parse_qs
    from urllib import quote


def generate_digest(secret, method, path, query, body):
    parsed_query = parse_qs(query, keep_blank_values=True)

    canonical_query = []

    for key in sorted(parsed_query.keys()):
        for value in sorted(parsed_query[key]):
            canonical_query.append("=".join((key, quote(value))))

    #print '### canonical_query'
    #print canonical_query

    string_to_hash = "n".join((method, path, "&".join(canonical_query), "")).encode("utf-8") + body
    #print '### string_to_hash, length: %s' % (len(string_to_hash))
    #print '### string_to_hash: %s' % string_to_hash
    #print '###'

    return hmac.new(
        secret.encode("utf-8"),
        string_to_hash,
        hashlib.sha256).hexdigest()
