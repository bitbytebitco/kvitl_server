#!/usr/bin/python

import os
import ssl
import json
import re
import jwt
import datetime
import csv, codecs, cStringIO
import logging
import tornado.ioloop
import tornado.web, tornado.httpserver
from tornado.web import HTTPError
from operator import itemgetter
from hmac_auth import hmac_authorized
import hmac
import ui_methods

import mysql.connector
from mysql.connector import errorcode

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

SITE_DIR = '/var/www/kvitl_server/'
CHECK_TABLES = False
KEY = 'my_key'
SECRET = 'my_secret_key'

######## not in use
jwtoptions = {
    'verify_signature': True,
    'verify_exp': True,
    'verify_nbf': False,
    'verify_iat': True,
    'verify_aud': False
}
########

TABLES = {}
TABLES['customers'] = (
    "CREATE TABLE `customers` ("
    "  `id` int(11) NOT NULL AUTO_INCREMENT,"
    "  `cust_name` varchar(40) NOT NULL,"
    "  `cust_email` varchar(40) NOT NULL,"
    "  `cust_total` decimal(6,2) NOT NULL,"
    "  PRIMARY KEY (`id`), UNIQUE KEY `id` (`id`)"
    ") ENGINE=InnoDB")

TABLES['products'] = (
    "CREATE TABLE `products` ("
    "  `id` int(11) NOT NULL AUTO_INCREMENT,"
    "  `prod_name` varchar(40) NOT NULL,"
    "  `prod_type` varchar(40) NOT NULL," # coffee club or coffee bar, or both? 
    "  PRIMARY KEY (`id`), UNIQUE KEY `id` (`id`)"
    ") ENGINE=InnoDB")

TABLES['product_options'] = (
    "CREATE TABLE `product_options` ("
    "  `id` int(11) NOT NULL AUTO_INCREMENT,"
    "  `prod_opt_parent_id` int(11)," # `id` of the parent `products` record if applicable
    "  `prod_opt_name` varchar(40) NOT NULL,"
    "  `prod_opt_price` varchar(40) NOT NULL,"
    "  PRIMARY KEY (`id`), UNIQUE KEY `id` (`id`)"
    ") ENGINE=InnoDB")


TABLES['transactions'] = (
    "CREATE TABLE `transactions` ("
    "  `id` int(11) NOT NULL AUTO_INCREMENT,"
    "  `trans_date` date NOT NULL,"
    "  `trans_method` varchar(40) NOT NULL,"
    "  `trans_total` decimal(6,2) NOT NULL,"
    "  PRIMARY KEY (`id`), UNIQUE KEY `id` (`id`)"
    ") ENGINE=InnoDB")

TABLES['transaction_items'] = (
    "CREATE TABLE `transaction_items` ("
    "  `trans_id` int(11) NOT NULL,"
    "  `prod_id` int(11) NOT NULL,"
    "  primary key (trans_id, prod_id), "  
    "  CONSTRAINT `transaction_items_ibfk_1` FOREIGN KEY (`trans_id`) "
    "     REFERENCES `transactions` (`id`)"
    ") ENGINE=InnoDB")

SQL = {}
''' customer related sql '''
SQL['create_customer'] = (
    "INSERT INTO customers "
    "(cust_name, cust_email, cust_total, newsletter_opt)"
    "VALUES (%(cust_name)s, %(cust_email)s, %(cust_total)s, cast(%(newsletter_opt)s as UNSIGNED))")

SQL['update_customer_by_id'] = (
    "UPDATE customers "
    #"set cust_name=%(cust_name)s, coffee_club=cast(%(coffee_club)s as UNSIGNED), newsletter_opt=cast(%(newsletter_opt)s as UNSIGNED) "
    "set cust_name=%(cust_name)s, coffee_club=0, newsletter_opt=cast(%(newsletter_opt)s as UNSIGNED) "
    "WHERE id=%(customer_id)s")

SQL['delete_customer_by_id'] = (
    "DELETE from customers where id=%(cust_id)s"
    )
SQL['mark_delete_customer_by_id'] = (
    "update customers set deleted = 1 where id=%(cust_id)s"
    )
SQL['select_customer_by_email'] = (
    "SELECT * from customers "
    "WHERE customers.cust_email = %(cust_email)s and deleted = 0")

SQL['select_customer_by_name'] = (
    "SELECT * from customers "
    "WHERE customers.cust_name = %(cust_name)s and deleted = 0")

SQL['select_customers_by_query'] = """SELECT id, cust_name, cust_email, CAST(TRUNCATE(cust_total,2) AS CHAR(3)) AS total, newsletter_opt, coffee_club from customers WHERE customers.cust_name LIKE %s and deleted=0"""

SQL['select_customer_by_id'] = (
    "SELECT id, cust_name, cust_email, CAST(TRUNCATE(cust_total,2) AS CHAR(3)) AS total , coffee_club, newsletter_opt from customers "
    "WHERE customers.id = %(cust_id)s and deleted =0")

SQL['select_all_customers'] = (
    "SELECT id, cust_name, cust_email, CAST(TRUNCATE(cust_total,2) AS CHAR(3)) AS total, newsletter_opt from customers where deleted = 0 order by cust_name")

''' product related sql '''
SQL['select_all_products'] = (
    "SELECT * from products order by prod_name")

SQL['create_product'] = (
    "INSERT INTO products "
    "(prod_name, prod_type)"
    "VALUES (%(prod_name)s, %(prod_type)s)")

SQL['update_product'] = (
    "UPDATE products "
    "set prod_name=%(name)s "
    "WHERE id=%(id)s"
    )

SQL['create_product_option'] = (
    "INSERT INTO product_options "
    "(prod_opt_parent_id, prod_opt_priority, prod_opt_name, prod_opt_price)"
    "VALUES (%(parent_id)s, %(priority)s, %(name)s, %(price)s)")

SQL['update_product_option'] = (
    "UPDATE product_options "
    "SET prod_opt_priority=%(priority)s, prod_opt_name=%(name)s, prod_opt_price=%(price)s "
    "WHERE id=%(opt_id)s" 
    )

SQL['delete_product_option'] = (
    "DELETE FROM product_options "
    "WHERE id = %(opt_id)s")

SQL['product_by_product_id'] = (
    "SELECT * from products where id=%(product_id)s"
    )

SQL['delete_product_by_id'] = (
    "DELETE from products where id=%(product_id)s"
    )

SQL['product_options_by_product_id'] = (
    "SELECT po.id, po.id as opt_id, p.prod_name, p.prod_type, po.prod_opt_name, po.prod_opt_priority, po.prod_opt_price "
    "from products p INNER JOIN product_options po ON p.id = po.prod_opt_parent_id "
    "where p.id=%(product_id)s order by po.prod_opt_priority, po.prod_opt_price")

SQL['select_all_products_with_options'] = (
    "select po.id as prod_opt_id, p.id as prod_id, p.prod_name, p.prod_type, po.prod_opt_priority, po.prod_opt_name, po.prod_opt_price "
    "from product_options po left join products p on po.prod_opt_parent_id = p.id where p.id is not null order by p.prod_name, po.prod_opt_priority")

SQL['create_transaction'] = (
    "INSERT into transactions (customer_id, trans_date, trans_method, trans_type, trans_total) VALUES (%(customer_id)s, NOW(), %(method)s, 'debit', %(total)s);")

SQL['delete_transaction_by_id'] = ("update transactions set deleted = 1 where id=%(trans_id)s")

''' credit account '''
SQL['credit_account'] = (
    "INSERT into transactions (customer_id, trans_date, trans_method, trans_type, trans_total) VALUES (%(customer_id)s, NOW(), %(method)s, 'credit', %(total)s);")

SQL['account_balance'] = (
    '''select t3.customer_id, cast(truncate((t2.credits-t3.debits),2) as char(12)) as balance from (select t1.customer_id, t1.credits from (select customer_id,sum(trans_total) AS credits from transactions where trans_type = "credit" and customer_id = %(customer_id)s and deleted=0) t1) t2 inner join (select customer_id, sum(trans_total) as debits from transactions where trans_type = 'debit' and customer_id = %(customer_id)s and deleted=0) t3 on t3.customer_id = t2.customer_id''')

SQL['account_credits_balance'] = (
    '''select customer_id, cast(truncate(sum(trans_total),2) as char(12)) as balance from transactions where trans_type = "credit" and customer_id=%(customer_id)s and deleted=0'''
)

SQL['acct_bal_counts'] = (
    '''select * from (select count(id) as credits from transactions where trans_type = "credit" and customer_id=%(customer_id)s and deleted=0) a, (select count(id) as debits from transactions where trans_type = "debit" and customer_id=%(customer_id)s and deleted=0) b'''
)

SQL['create_transaction_item'] = (
    "INSERT into transaction_items (trans_id, prod_id) VALUES (%(trans_id)s, %(prod_id)s);")

SQL['select_all_transactions'] = ("SELECT t.id, c.cust_name, t.trans_date, t.trans_type, t.trans_total from transactions t inner join customers c on c.id = t.customer_id ORDER BY UNIX_TIMESTAMP(trans_date) DESC")

SQL['get_transactions_by_customer_id'] = ("select id, customer_id, trans_date, trans_type, cast(trans_total as char(5)) as trans_total from transactions where customer_id = %(customer_id)s and deleted=0 order by trans_date desc;")

class UnicodeWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        # Redirect output to a queue
        self.queue = cStringIO.StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):
        self.writer.writerow([s.encode("utf-8") for s in row])
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        # ... and reencode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        self.stream.write(data)
        # empty queue
        self.queue.truncate(0)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)

class DB(object):
    def __init__(self):
        try:
            self.cnx = mysql.connector.connect(user='kvitl_test', password='password',
                          host='127.0.0.1',
                          database='kvitl')
            self.cursor = self.cnx.cursor(dictionary=True)
        except Exception as e:
            print(e)

    def _connect(self):
        try:
            self.cnx = mysql.connector.connect(user='kvitl_test', password='password',
                          host='127.0.0.1',
                          database='kvitl')
            self.cursor = self.cnx.cursor(dictionary=True)
        except Exception as e:
            print(e  )
            print('connect problem')

    def check_tables_installed(self):
        self.cursor.execute("SHOW TABLES")
        db_tables = self.cursor.fetchall() 
        print('check_tables' )
        print(db_tables)
        existing = [i[0] for i in db_tables]
        print(existing)

        # TODO: need to add checking for partial table creation, db schema version checking?
        not_installed = []
        for table_name in TABLES:
            print(table_name)
            if table_name not in existing:
                not_installed.append(table_name)
        
        #not_installed = sorted(not_installed)
        not_installed.sort(key=lambda item: (len(item), item))

        print(not_installed )
        if len(not_installed)>0:
            print('installing tables')
            for table_name in not_installed:
                try:
                    self.create_table(table_name)             
                except Exception as e:
                    print(e)

    def create_table(self, db_name):
        try:
            if db_name in TABLES:
                self.cursor.execute(TABLES[db_name])
                self.cnx.commit()
        except mysql.connector.Error as err:
            print("Failed creating database: {}".format(err))
            #exit(1)       

    def customer_exists_by_email(self, cust_email):
        customer = self.select_customer_by_email(cust_email)
        if customer is not None:
            return True
        else:
            return False 

    def customer_exists_by_name(self, cust_name):
        customer = self.select_customer_by_name(cust_name)
        if customer is not None:
            return True
        else:
            return False 

    def select_customer_by_email(self, cust_email):
        try:
            self.cursor.execute(SQL['select_customer_by_email'], cust_email) 
            customers = self.cursor.fetchall() 
            if len(customers)>0:
                return customers[0] 
            else:
                return None
        except Exception as e:
            print(e)

    def select_customer_by_name(self, cust_name):
        try:
            self.cursor.execute(SQL['select_customer_by_name'], cust_name) 
            customers = self.cursor.fetchall() 
            if len(customers)>0:
                return customers[0] 
            else:
                return None
        except Exception as e:
            print(e)

    def select_customer_by_id(self, cust_id):
        try:
            data = { 'cust_id':cust_id }
            self.cursor.execute(SQL['select_customer_by_id'], data) 
            customers = self.cursor.fetchall() 
            if len(customers)>0:
                return customers[0] 
            else:
                return None
        except Exception as e:
            print(e)
        return None

    def select_customers_by_query(self, query):
        try:
            q = "%" + query + "%"  
            print(q)
            self.cursor.execute(SQL['select_customers_by_query'], (q, ))
            customers = self.cursor.fetchall() 
            if len(customers)>0:
                return customers 
            else:
                return None
        except Exception as e:
            print(e)

    def select_all_customers(self):
        try:
            self.cursor.execute(SQL['select_all_customers']) 
            customers = self.cursor.fetchall() 
            if len(customers)>0:
                return customers 
            else:
                return [] 
        except Exception as e:
            print(e)
            print('happened here')
            return None

    def select_all_products(self):
        try:
            self.cursor.execute(SQL['select_all_products']) 
            products = self.cursor.fetchall() 
            if len(products)>0:
                return products 
            else:
                return [] 
        except Exception as e:
            print(e)
            print('happened here')
            return None

    def select_all_products_with_options(self):
        try:
            self.cursor.execute(SQL['select_all_products_with_options']) 
            products_options = self.cursor.fetchall() 
            if len(products_options)>0:
                p_ids = [] 
                products_list = []
                for po in products_options:
                    #print po
                    
                    product_option = {}
                    product_option['prod_opt_id'] = po['prod_opt_id']
                    product_option['prod_opt_parent_id'] = po['prod_id']
                    product_option['prod_opt_priority'] = po['prod_opt_priority']
                    product_option['prod_opt_name'] = po['prod_opt_name']
                    product_option['prod_opt_price'] = po['prod_opt_price']

                    if po['prod_id'] not in p_ids:
                        prod = {}
                        prod['prod_id'] = po['prod_id']
                        prod['prod_name'] = po['prod_name']
                        prod['prod_type'] = po['prod_type']
                        prod['options'] = []
                        prod['options'].append(product_option)
                        products_list.append(prod)
                        p_ids.append(po['prod_id'])
                    else:
                        for pitem in products_list:
                            if pitem['prod_id'] == po['prod_id']:
                                pitem['options'].append(product_option)

                return products_list 
            else:
                return None
        except Exception as e:
            print(e)
            print('happened here')
            return None

    def product_by_product_id(self, product_id):
        try:
            data = { 'product_id' : product_id }
            self.cursor.execute(SQL['product_by_product_id'], data) 
            product = self.cursor.fetchall() 
            if len(product)>0:
                return product[0]
            else:
                return None
        except Exception as e:
            print(e)
            print('happened here')
            self._connect()
            return None

    def delete_product_by_id(self, product_id):
        try:
            data = { 'product_id' : product_id }
            self.cursor.execute(SQL['delete_product_by_id'], data) 
            self.cnx.commit()
        except Exception as e:
            print(e)
            return False 

    def product_options_by_product_id(self, product_id):
        try:
            data = { 'product_id' : product_id }
            self.cursor.execute(SQL['product_options_by_product_id'], data) 
            product_options = self.cursor.fetchall() 
            if len(product_options)>0:
                return product_options 
            else:
                return None
        except Exception as e:
            print(e)
            print('happened here')
            return None

    def update_product_option(self, opt_data):
        try:
            print('trying')
            self.cursor.execute(SQL['update_product_option'], opt_data)
            self.cnx.commit()
        except Exception as e:
            print(e)
            self.cnx.rollback()
            return False
            print(e)

    def update_product(self, data):
        try:
            self.cursor.execute(SQL['update_product'], data)
            self.cnx.commit()
        except Exception as e:
            print(e)
            self.cnx.rollback()
            return False
            print(e)

    def create_product(self, prod_data, prod_opts=None):
        try:
            print('inserting product')
            print(prod_data)
            self.cursor.execute(SQL['create_product'], prod_data)
            self.cnx.commit()
        except Exception as e:
            self.cnx.rollback()
            return False
            print(e)
  
        parent_id = self.cursor.lastrowid 
        print('parent_id')
        print(parent_id)
        if prod_opts is not None: 
            for opt_id in prod_opts:
                prod_opts[opt_id]['parent_id'] = parent_id
                prod_opts[opt_id]['priority'] = opt_id 
                self.create_product_option(prod_opts[opt_id])

    def create_product_option(self, option):
        try:
            self.cursor.execute(SQL['create_product_option'], option)
            self.cnx.commit()
        except Exception as e:
            self.cnx.rollback()
            print(e)

    def delete_product_option(self, opt_id):
        try:
            option = {'opt_id':opt_id}
            self.cursor.execute(SQL['delete_product_option'], option)
            self.cnx.commit()
        except Exception as e:
            self.cnx.rollback()
            print(e)

    def create_customer(self, data_cust):
        try:
            print('inserting customer')
            print(data_cust)
            self.cursor.execute(SQL['create_customer'], data_cust)
            self.cnx.commit()
        except Exception as e:
            self.cnx.rollback()
            print(e)

    def update_customer_by_id(self, data_cust):
        try:
            print('updating customer')
            print(data_cust)
            self.cursor.execute(SQL['update_customer_by_id'], data_cust)
            self.cnx.commit()
        except Exception as e:
            self.cnx.rollback()
            print(e)

    def delete_customer_by_id(self, cust_id):
        try:
            data = { 'cust_id' : cust_id }
            self.cursor.execute(SQL['mark_delete_customer_by_id'], data) 
            self.cnx.commit()
        except Exception as e:
            print(e)
            return False 

    def acct_bal_counts(self, customer_id):
        try:    
            data = {'customer_id':customer_id}
            self.cursor.execute(SQL['acct_bal_counts'], data) 
            balance = self.cursor.fetchall() 
            if len(balance)>0:
                return balance[0]
                return None
        except Exception as e:
            return False 

    def account_balance(self, customer_id):
        try:    
            data = {'customer_id':customer_id}
            self.cursor.execute(SQL['account_balance'], data) 
            balance = self.cursor.fetchall() 
            if len(balance)>0:
                return balance[0] 
            else:
                return None
        except Exception as e:
            print(e)
            return False

    def account_credits_balance(self, customer_id):
        try:    
            data = {'customer_id':customer_id}
            self.cursor.execute(SQL['account_credits_balance'], data) 
            balance = self.cursor.fetchall() 
            if len(balance)>0:
                return balance[0] 
            else:
                return None
        except Exception as e:
            print(e)
            return False

    def credit_account(self, data):
        try:    
            self.cursor.execute(SQL['credit_account'], data) 
            self.cnx.commit()
        except Exception as e:
            print(e)
            return False 

    def delete_transaction_by_id(self, trans_id):
        try:
            data = { 'trans_id' : trans_id }
            self.cursor.execute(SQL['delete_transaction_by_id'], data) 
            self.cnx.commit()
        except Exception as e:
            print(e)
            return False 

    def create_transaction(self, data, items):
        try:    
            self.cursor.execute(SQL['create_transaction'], data) 
            self.cnx.commit()
        except Exception as e:
            print(e)
            return False 
        
        transaction_id = self.cursor.lastrowid 
        try:    
            for prod_id in items:
                d = {
                    'trans_id':transaction_id,
                    'prod_id':prod_id
                }
                self.cursor.execute(SQL['create_transaction_item'], d) 
                self.cnx.commit()
        except Exception as e:
            print(e)
            return False 
        
    def get_transactions(self):
        try:
            self.cursor.execute(SQL['select_all_transactions']) 
            transactions = self.cursor.fetchall() 
            return transactions
        except Exception as e:
            print(e)
            return False

    def get_transactions_by_customer_id(self, customer_id):
        try:
            data = {'customer_id':customer_id}
            self.cursor.execute(SQL['get_transactions_by_customer_id'], data) 
            transactions = self.cursor.fetchall() 
            return transactions
        except Exception as e:
            print(e)
            return False

class BaseHandler(tornado.web.RequestHandler):

    def get_current_user(self):
        return self.get_secure_cookie("user")

    ''' Base Handler '''
    def initialize(self):
        self.db = DB() 
    def on_finish(self):
        self.db.cnx.close()

class BaseWebHandler(BaseHandler):
    def prepare(self):
        if not self.current_user:
            self.redirect("/login")
            return    

class ApiBaseHandler(BaseHandler):
    def get_secret(self, key):
        if key == KEY:
            return SECRET

    @hmac_authorized
    def prepare(self):
        print("Remote_ip: %s " % self.request.remote_ip)
        pass
        
    '''
    def _return_forbidden(self, e):
        self.set_header('Content-Type', 'application/json')
        self.set_status(401)
        self.finish(json.dumps({'error': str(e)}))
    def prepare(self):
        try:
            token = self.get_argument('access_token', None)
            if not token:
                auth_header = self.request.headers.get('Authorization', None)
                if not auth_header:
                    raise Exception('This resource need a authorization token')
                token = auth_header[7:]
                try:
                    jwt.decode(
                        token,
                        SECRET,
                        options=jwtoptions
                    )
                except Exception as e:
                    print(e)
                    raise Exception(e.message)
        except Exception as e:
            print(e)
            self._return_forbidden(e)
    '''

class MainHandler(BaseWebHandler):
    ''' Main '''
    def get(self):
        self.redirect('/customers')
        #self.render("index.html")
        #self.write('ok')

class CustomersHandler(BaseWebHandler):
    ''' Customers '''
    def get(self):
        db = self.db 
        customers = db.select_all_customers()
        #self.write(json.dumps(customers))
        self.render("customers.html", customers=customers)

class CustomersLoadHandler(BaseWebHandler):
    ''' Load Customers '''
    def get(self):
        db = self.db 
 
        count = 0 
        with open('csv/rostovs_customers.csv', 'rb') as f:
            reader = csv.reader(f)
            for row in reader:
                if count>0:
                    print(count)
                    cust_name = "%s %s " % (row[0], row[1])
                    print(cust_name)
                    existing = db.customer_exists_by_name({'cust_name':cust_name}) 
                    if existing is False:
                        print('doesnt exist')
                        cust = {
                            'cust_name':cust_name,
                            'cust_email':"",
                            'cust_total':0,
                            'newsletter_opt':0
                        }
                        db.create_customer(cust)
                count = count + 1
        self.write('ok')
        #customers = db.select_all_customers()
        #self.write(json.dumps(customers))

        #self.redirect('/')

#class ApiCustomersHandler(BaseHandler):
class ApiCustomersHandler(ApiBaseHandler):
    ''' Api: Customers '''
    @hmac_authorized
    def get(self):
        print('api customers')
        db = self.db 
        customers = db.select_all_customers()
        #print re.search('(\s.(.*)|(.*))', "bob franks").group(0).strip()
        #print re.search('(?(?=\s(.*))\s.(.*)|(.*))', "bob franks").group(0)
        #print sorted(customers, key=re.search('\s.(.*)', itemgetter('cust_name')).group(0).strip())
        self.write(json.dumps(customers))

class ApiCustomersCSVHandler(BaseHandler):
    def get(self):
        print('api customers: csv')
        db = self.db 
        customers = db.select_all_customers()
        try:
            fn = os.path.join(SITE_DIR, 'csv/customers.csv')
            #logger.info(fn) 
            if os.path.exists(fn):
                os.remove(fn)
            #with open(r'csv/customers.csv', 'wb') as csvfile:
            with open(fn, 'wb') as csvfile:
                uw = UnicodeWriter(csvfile, delimiter=',',
                    quotechar='"', quoting=csv.QUOTE_MINIMAL)
                for row in customers:
                    uw.writerow([unicode(row['cust_name']), unicode(row['cust_email']), unicode(row['newsletter_opt'])])
        except Exception as e:
            logging.info(e)
        finally: 
            if os.path.exists(fn):
                self.write({'result':'ok'})

class ApiCustomerHandler(ApiBaseHandler):
    ''' Api: Customer '''
    def get(self, customer_id):
        print('api customer')
        db = self.db 
        customer = db.select_customer_by_id(customer_id)
        print(customer)
        self.write(json.dumps(customer))

class ApiCustomersQueryHandler(ApiBaseHandler):
    ''' Api: Customer by query'''
    def get(self, query):
        print('api customer by query')
        db = self.db 
        
        customers = db.select_customers_by_query(query)
        print(customers)
        self.write(json.dumps(customers))

class ApiCustomerCreateHandler(ApiBaseHandler):
    ''' Api Customer Create '''
    def write_error(self, status_code, **kwargs):
        self.finish(json.dumps({
            'error': {
                'code': status_code,
                'message': self._reason,
            }
        }))

    #def check_xsrf_cookie(self):
    #    return 

    def get(self):
        print(self.xsrf_token )
        print('get api customer create')
        print('')
        self.write('ok')

    def post(self):
        db = self.db
        print('')
        print('post')
        print(self.request.headers)
        print(self.request.arguments)
        name = self.get_argument("customer_name")
        email = self.get_argument("customer_email")
        newsletter = self.get_argument("newsletter_opt")
        if name is not None and email is not None and len(email)>0 and newsletter is not None:
            existing = db.customer_exists_by_email({'cust_email':email}) 
            print('existing')
            print(existing)
            if existing is False:
                cust = {
                    'cust_name':name,
                    'cust_email':email,
                    'cust_total':0,
                    'newsletter_opt':newsletter
                }
                db.create_customer(cust)
                self.write(json.dumps({'result':'ok'}))
            else:
                self.set_status(501)
                self.write(json.dumps({'error':'A customer with this email already exists.'})) 
        elif name is not None and not(len(email)>0) and newsletter is not None:
            print('trying with no email')
            existing = db.customer_exists_by_name({'cust_name':name}) 
            if existing is False:
                cust = {
                    'cust_name':name,
                    'cust_email':"",
                    'cust_total':0,
                    'newsletter_opt':newsletter
                }
                db.create_customer(cust)
                self.write(json.dumps({'result':'ok'}))
            else:
                self.set_status(501)
                self.write(json.dumps({'error':'A customer with this name already exists.'}))
        else:
            #raise HTTPError(501)
            self.set_status(501)
            self.write(json.dumps({'error':'An issue occurred trying to create this customer.'})) 

class CustomerCreateHandler(BaseWebHandler):
    ''' Customer Create '''
    def write_error(self, status_code, **kwargs):
        self.finish(json.dumps({
            'error': {
                'code': status_code,
                'message': self._reason,
            }
        }))

    def get(self):
        self.render("customer_create.html")

    def post(self):
        db = self.db
        print('post')
        print(self.request.arguments )
        post_args = ["customer_name", "customer_email"]
        for arg in self.request.arguments:
            if arg in post_args:
                print(self.request.arguments[arg])

        name = self.get_argument("customer_name")
        email = self.get_argument("customer_email")
        try: 
            newsletter = self.get_argument("newsletter_opt")
        except Exception as e:
            newsletter = 0
        if name is not None and email is not None and len(email)>0:
            existing = db.customer_exists_by_email({'cust_email':email}) 
            print('existing')
            print(existing)
            if existing is False:
                cust = {
                    'cust_name':name,
                    'cust_email':email,
                    'cust_total':0, 
                    'newsletter_opt':newsletter 
                }
                db.create_customer(cust)
                self.redirect('/customers')
        elif name is not None and not(len(email)>0):
            print('trying with no email')
            existing = db.customer_exists_by_name({'cust_name':name}) 
            if existing is False:
                cust = {
                    'cust_name':name,
                    'cust_email':"",
                    'cust_total':0,
                    'newsletter_opt':newsletter
                }
                db.create_customer(cust)
                self.redirect('/customers')
            else:
                self.set_status(501)
                self.write(json.dumps({'error':'A customer with this email already exists.'})) 
        else:
            self.set_status(501)
            self.write(json.dumps({'error':'An issue occurred trying to create this customer.'})) 

class CustomerDeleteHandler(BaseWebHandler):
    ''' Customer Delete '''
    def post(self):
        print(self.request.arguments)
        cust_id = self.get_argument('customer_id') 
        print(cust_id)
        self.db.delete_customer_by_id(cust_id)
        self.write(json.dumps({'result':'ok'}))

class ApiCustomerDeleteHandler(ApiBaseHandler):
    def get(self, cust_id):
        print('mark customer deleted')
        self.db.delete_customer_by_id(cust_id)
        self.write(json.dumps({'result':'ok'}))
        self.write('ok')

class CustomerEditHandler(BaseHandler):
    ''' Customer Edit '''
    def get(self, cust_id):
        customer = self.db.select_customer_by_id(cust_id)
        print(customer)
        return self.render('customer_edit.html', customer=customer)

    def post(self, cust_id):
        customer_id = self.get_argument("customer_id")
        if customer_id is not None:
            name = self.get_argument("customer_name")
            data = {
                'customer_id':customer_id,
                'cust_name':name,
            }
            if 'coffee_club' in self.request.arguments and self.request.arguments['coffee_club']:
                data['coffee_club'] = 1 
            else:
                data['coffee_club'] = 0 
            if 'newsletter_opt' in self.request.arguments:
                data['newsletter_opt'] = 1 
            else:
                data['newsletter_opt'] = 0 
            print(data )
            self.db.update_customer_by_id(data)
            self.redirect('/customers')

class ApiCustomerEditHandler(ApiBaseHandler):
    ''' Customer Edit '''
    def prepare(self):
        print(''' ########################### Customer Edit ''')
    def get(self):
        print(self.xsrf_token )
        self.write('ok')

    def post(self):
        print('')
        print('##### ApiCustomerEditHandler post')
        print(self.request.arguments)
        customer_id = self.get_argument('customer_id')
        if customer_id is not None:
            name = self.get_argument("customer_name")
            data = {
                'customer_id':customer_id,
                'cust_name':name,
            }
            #if 'coffee_club' in self.request.arguments:
            #    data['coffee_club'] = self.get_argument('coffee_club') 
            if 'newsletter_opt' in self.request.arguments:
                data['newsletter_opt'] = self.get_argument('newsletter_opt') 

            print(data )
            self.db.update_customer_by_id(data)
            self.write(json.dumps({'result':'ok'}))
        else: 
            print('something went wrong')
            

class ProductCreateHandler(BaseWebHandler):
    def get(self):
        self.render("product_create.html")
    def post(self):
        print('post')
        print(self.request.arguments    )
           
        prod_data = {
            'prod_name': self.get_argument("prod_name"),
            'prod_type':'coffee_bar'
        }
        
        prod_opts = self._get_product_options(self.request.arguments)

        print('prod_opts')
        print(prod_opts)
        self.db.create_product(prod_data, prod_opts)

        #self.write(json.dumps(self.request.arguments))
        self.redirect('/products')

    

    def _get_product_options(self, args):
        print(args    )
        options = {} 
        for opt in args:
            if bool(re.search(r'\d', opt)) and not(bool(re.search(r'del', opt))):
                print('')
                print('opt found')
                opt_index = re.match('.*?([0-9]+)$', opt).group(1) 

                print(opt)
                print(args[opt][0])
                print(opt_index)

                if opt_index not in options:
                    options[opt_index] = {}
                if 'option' in opt:
                    opt_name = re.match('option-(.*)-\d+', opt).group(1)
                    print(opt_name )
                    options[opt_index][opt_name] = args[opt][0]
        if len(options)>0:
            return options
        return None 


class ProductsHandler(BaseWebHandler):
    ''' Products '''
    def get(self):
        db = self.db
        print('1')
        products = db.select_all_products()
        print('2')
        self.render('products.html', products=products)
        #self.write(json.dumps(products))

class ApiProductsHandler(BaseHandler):
    def get(self):
        db = self.db
        print('ApiProductsHandler')
        products = db.select_all_products()
        self.write(json.dumps(products))

class ApiProductsWithOptionsHandler(ApiBaseHandler):
    def get(self):
        db = self.db
        print('ApiProductsWithOptionsHandler')
        products = db.select_all_products_with_options()
        self.write(json.dumps(products))

class ApiProductOptionsHandler(BaseHandler):
    def get(self, prod_id):
        print('ApiProductOptionsHandler')
        if prod_id is not None:
            prod_opts = self.db.product_options_by_product_id(prod_id)
            self.write(json.dumps(prod_opts))
        else:
            self.write(json.dumps({'result':'missing_data'}))

class ProductDeleteHandler(BaseWebHandler):
    ''' Product Delete '''
    def post(self):
        print(self.request.arguments)
        prod_id = self.get_argument('product_id') 
        print(prod_id)
        self.db.delete_product_by_id(prod_id)
        self.write(json.dumps({'result':'ok'}))

class ProductEditHandler(BaseWebHandler):
    ''' Product Edit '''
    def get(self, prod_id):
        print(prod_id)
        print(self.request.arguments)
        prod = self.db.product_by_product_id(prod_id)
        prod_opts = self.db.product_options_by_product_id(prod_id)
        print(prod_opts)
        print(prod)
        self.render("product_edit.html", prod_id=prod_id, prod=prod, prod_opts=prod_opts)

    def post(self, prod_id):
        try:
            opts = self._get_product_options(self.request.arguments)
            opts_to_del = self._get_delete_options(self.request.arguments) 
            ids_from_post = []
            
            print('')
            print('opts' )
            print('all args')
            print(self.request.arguments)

            # update main product 
            if opts is not None:
                print(opts )
                name = self.get_argument('prod_name') 
                print('nametest')
                print(name)
                data = {
                    'id':prod_id,
                    'name':name
                }
                self.db.update_product(data)

                print('update existing')
                print(opts)
                # update existing product options
                count = 0
                for opt_id in opts:
                    count = count + 1
                    data = {
                        'opt_id':opt_id,
                        'priority':count,
                        'name':opts[opt_id]['name'],
                        'price':opts[opt_id]['price']
                    }
                    ids_from_post.append(int(opt_id))
                    self.db.update_product_option(data)

            # add new options
            existing_options = self.db.product_options_by_product_id(prod_id)
            if existing_options is not None:
                existing_ids = [x['opt_id'] for x in existing_options]
            else:
                existing_ids = []
            new_ids = [x for x in ids_from_post if x not in existing_ids]

            print(new_ids)
            print('new options')
            if opts is not None:
                for opt_id in opts:
                    if int(opt_id) in new_ids:
                        count = count + 1
                        print(opts[opt_id])
                        new_opt = {
                            'parent_id':prod_id,
                            'priority':count,
                            'name':opts[opt_id]['name'],
                            'price':opts[opt_id]['price'] 
                        }
                        self.db.create_product_option(new_opt)
                        print(new_opt)

            # delete options
            #to_delete = [x for x in existing_ids if x not in ids_from_post] 
            # ^ deprecated
            
            print('to_delete new')
            print(opts_to_del )
            for opt_id in opts_to_del:
                self.db.delete_product_option(opt_id)

            #self.redirect('/products')
            self.write({'result':'ok'})
        except Exception as e:
            print(e)
            self.write({'result':'error'})
    
    def _get_delete_options(self, args):
        options_to_del = [] 
        for opt in args:
            if bool(re.search(r'del', opt)):
                print('del options')
                print(opt)
                opt_index = re.match('.*?([0-9]+)$', opt).group(1) 
                options_to_del.append(opt_index)
        return options_to_del 

    def _get_product_options(self, args):
        ''' Matches options with names like option-[name]-[id] whose arg value is the value of option '''
        
        print('get_product_options')
        options = {} 
        print(args)

        for opt in args:
            if bool(re.search(r'\d', opt)) and not(bool(re.search(r'del', opt))):
                opt_index = re.match('.*?([0-9]+)$', opt).group(1) 

                #print opt
                #print args[opt][0]
                #print opt_index

                if opt_index not in options:
                    options[opt_index] = {}
                if 'option' in opt:
                    opt_name = re.match('option-(.*)-\d+', opt).group(1)
                    #print opt_name 
                    options[opt_index][opt_name] = args[opt][0]
        print(options)
        if len(options)>0:
            return options
        return None 

class TransactionsHandler(BaseWebHandler):
    def get(self):
        transactions = self.db.get_transactions()
        self.render('transactions.html', transactions=transactions)

class ApiTransactionsCSVHandler(BaseHandler):
    def get(self):
        transactions = self.db.get_transactions()
        try:
            fn = os.path.join(SITE_DIR, 'csv/transactions.csv')
            if os.path.exists(fn):
                os.remove(fn)
            with open(fn, 'wb') as csvfile:
                uw = UnicodeWriter(csvfile, delimiter=',',
                    quotechar='"', quoting=csv.QUOTE_MINIMAL)
                for row in transactions:
                    if row['trans_type'] == "debit":
                        trans_total = "(%s)" % unicode(row['trans_total'])
                    else: 
                        trans_total = unicode(row['trans_total']) 
                    uw.writerow([unicode(row['cust_name']),unicode(row['trans_date']), unicode(row['trans_type']), trans_total])
        finally: 
            if os.path.exists(fn):
                self.write({'result':'ok'})

class ApiTransactionsHandler(ApiBaseHandler):
    def datetostring(self,o):
        if isinstance(o, datetime.datetime):
            return o.__str__()

    def get(self, customer_id):
        print('ApiTransactionsHandler')
        transactions = self.db.get_transactions_by_customer_id(customer_id)
        #print transactions
        self.write(json.dumps(transactions, default=self.datetostring)) 

class ApiBalanceHandler(ApiBaseHandler):
    def get(self, customer_id):
        print('ApiBalanceHandler')
        print(customer_id)
        resp1 = self.db.acct_bal_counts(customer_id)
        if resp1['credits']>0:
            print('more than 0 credits')
            if resp1['debits']>0:
                resp = self.db.account_balance(customer_id)
            else:
                resp = self.db.account_credits_balance(customer_id)
            print('####')
            print(resp )
            self.write(json.dumps(resp))
        else:
            print('no credits')
            json_resp = {
                'customer_id': customer_id,
                'balance': "0.00" 
            }
            self.write(json.dumps(json_resp))

class ApiCreditHandler(ApiBaseHandler):
    def get(self):
        print(self.xsrf_token )
        self.write('ok')

    def post(self):
        print(self.request.arguments)
        try:
            customer_id = self.get_argument('customer_id')
            amount = self.get_argument('amount')
            data = { 
                'customer_id':customer_id, 
                'method':'cash', 
                'total':amount 
            }
            self.db.credit_account(data)
        except Exception as e:
            print(e)
        self.write(json.dumps({'result':'ok'}))

class TransactionCreateHandler(BaseWebHandler):
    def get(self):
        self.render('transaction_create.html')
    def post(self):
        print(self.request.arguments)
        try:
            customer_id = self.get_argument('customer_id')
            total = self.get_argument('total')
            data = { 
                'customer_id':customer_id, 
                'method':'cash', 
                'total':total 
            }
            items = [9,13]
            self.db.create_transaction(data, items)
        except Exception as e:
            print(e)
        self.write('ok')  

class ApiTransactionCreateHandler(ApiBaseHandler):
    def get(self):
        print(self.xsrf_token )
        self.write('ok')   
    def post(self):
        print(self.request.arguments)
        try:
            customer_id = self.get_argument('customer_id')
            total = self.get_argument('total')
            item_arr = self.get_arguments('items', strip=True)
            print('####' )
            print('####' )
            items = []
            for i in item_arr:
                items.append(i)
            data = { 
                'customer_id':customer_id, 
                'method':'cash', 
                'total':total 
            }
            print(items)
            self.db.create_transaction(data, items)
        except Exception as e:
            print(e)
        self.write({'result':'ok'})

class ApiTransactionDeleteHandler(ApiBaseHandler):
    def get(self):
        print(self.xsrf_token )
        self.write('ok')
    def post(self):
        print(''' ApiTransactionDeleteHandler ''')
        print(self.request.arguments)
        try:
            trans_id = self.get_argument('transaction_id')
            print("attempting to delete: %s" % trans_id)
            self.db.delete_transaction_by_id(trans_id)
            self.write({'result':'ok'})
        except Exception as e:
            print(e)
            self.write({'result':'error'})

class HmacTestHandler(tornado.web.RequestHandler):
    @hmac_authorized
    def get(self):
        self.write('test')

class AuthHandler(tornado.web.RequestHandler):
    def prepare(self):
        self.set_header('Content-Type', 'application/json')
        print(self.xsrf_token )
        self.encoded = jwt.encode({
            'some': 'payload',
            'a': {2: True},
            'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=300)},
            SECRET,
            algorithm='HS256'
        )

    def get(self):
        response = {'token': self.encoded}
        self.finish(json.dumps(response))


class LoginHandler(BaseHandler):
    def get(self):
        self.render('login.html')

    def post(self):
        names = ['rostovs']
        name = self.get_argument("username")
        key = self.get_argument("password")
        if name in names and key in ["porkchop"]:
            self.set_secure_cookie("user", name)
            self.redirect("/")
        else:
            self.redirect("/login")
           
class LogoutHandler(BaseHandler):
    def get(self):
        self.clear_cookie("user")
        self.redirect("/")
 
def make_app():
    settings = {
        "cookie_secret": "__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__", 
        "xsrf_cookies": True,
    }
    print('ui_methods')
    print(dir(ui_methods))
    return tornado.web.Application([
        (r"/", MainHandler),
        (r"/login", LoginHandler),
        (r"/logout", LogoutHandler),
        (r'/customer/create', CustomerCreateHandler),
        (r'/customer/delete', CustomerDeleteHandler),
        (r'/customers', CustomersHandler),
        (r'/customers/load', CustomersLoadHandler),
        (r'/api/customers', ApiCustomersHandler),
        (r'/api/customers/csv', ApiCustomersCSVHandler),
        (r'/api/customer/(\d+)', ApiCustomerHandler),
        (r'/api/customers/q/(.*)', ApiCustomersQueryHandler),
        (r'/api/customer/create', ApiCustomerCreateHandler),
        (r'/api/customer/balance/(\d+)', ApiBalanceHandler),
        (r'/api/customer/delete/(\d+)', ApiCustomerDeleteHandler),
        (r'/api/customer/edit', ApiCustomerEditHandler),
        (r'/customer/edit/(\d+)', CustomerEditHandler),
        (r'/product/edit/(\d+)', ProductEditHandler),
        (r'/product/delete', ProductDeleteHandler),
        (r'/product/create', ProductCreateHandler),
        (r'/products', ProductsHandler),
        (r'/api/products', ApiProductsHandler),
        (r'/api/products_with_options', ApiProductsWithOptionsHandler),
        (r'/api/product/options/(\d+)', ApiProductOptionsHandler),
        (r'/transaction/create', TransactionCreateHandler),
        (r'/api/transactions/(\d+)', ApiTransactionsHandler),
        (r'/api/transaction/create', ApiTransactionCreateHandler),
        (r'/api/transaction/delete', ApiTransactionDeleteHandler),
        (r'/api/transactions/csv', ApiTransactionsCSVHandler),
        (r'/transaction/credit', ApiCreditHandler),
        (r'/transactions', TransactionsHandler),
        (r'/api/auth', AuthHandler),
        #(r'/api/auth/test', TestAuthHandler),
        (r'/api/hmac_test', HmacTestHandler),
        (r'/css/(.*)', tornado.web.StaticFileHandler, {'path': SITE_DIR + "/css/"}),
        (r'/js/(.*)', tornado.web.StaticFileHandler, {'path': SITE_DIR + "/js/"}),
        (r'/file/(.*)', tornado.web.StaticFileHandler, {'path': SITE_DIR + "/files/"}),
        (r'/csv/(.*)', tornado.web.StaticFileHandler, {'path': SITE_DIR + "/csv/"}),
    ], ui_methods=ui_methods, debug=True, **settings)


if __name__ == "__main__":
    if CHECK_TABLES:
        db = DB()
        db.check_tables_installed()

    ssl_options = { 
        "certfile":"/etc/letsencrypt/live/diluo.org/fullchain.pem",
        "keyfile":"/etc/letsencrypt/live/diluo.org/privkey.pem",
        #"cert_reqs":ssl.CERT_REQUIRED,
        #"ca_certs": "/etc/ssl/certs/tornado/2017/diluo_org.ca-bundle",
        "ssl_version": ssl.PROTOCOL_TLSv1
    } 
    http_server = tornado.httpserver.HTTPServer(
        make_app(),
        ssl_options=ssl_options,    
    )
    http_server.listen(10000) 
    #app = make_app()
    #app.listen(200)
    tornado.ioloop.IOLoop.current().start()
