#!/usr/bin/python

import requests
from digest import generate_digest

KEY = "my_key"
SECRET = "my_secret_key"

host = 'https://diluo.org:300'

algorithm = "KVITL-V1"
method = "GET"
path = '/api/customers'
query = ""
body = ""

digest = generate_digest(SECRET, method, path, query, body)

auth_header = "%s %s %s" % (algorithm, KEY, digest)
#print auth_header
url = host + path
r = requests.get(url , headers={"Authorization":auth_header})
print r.text
