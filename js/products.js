$(document).ready(function(){
    $('.edit_product').on('click', function(){
        console.log('edit product');
        var prod_id = $(this).attr('ref'); 
        console.log(prod_id);
        location.href = "/product/edit/" + prod_id;
    }); 
    $('.new_product').on('click', function(){
        location.href = '/product/create'; 
    });
    $('.delete_product').on('click', function(){
        console.log('delete product');
        var prod_id = $(this).attr('ref'); 
        console.log(prod_id);
        var prod_name = $(this).closest('tr').find('td:first').text();
        $('#prod_name').text(prod_name);
        $('#prod_id').val(prod_id);
        //console.log(prod_name);
        $('#delete_modal').modal('show');

    });
    $('#confirm_delete').on('click', function(){
        var prod_id = $('#prod_id').val(); 
        console.log(prod_id);
        delete_product(prod_id);        
    }); 
});

function delete_product(prod_id){
    $.post('/product/delete', { product_id: prod_id, _xsrf: $('input[name="_xsrf"]').val() }, function(data){
        console.log(data);
        var d = JSON.parse(data);
        if(d['result'] == 'ok'){
            location.href = '/products';
        }
    });
}
