$(document).ready(function(){
    deleted = [];

    // add option handler
    $('#add_option_button').on('click', function() {
        console.log('add option');
        var name = $('#option-name').val();
        var price = $('#option-price').val();
        add_option(name, price);
        return false;
    });  
   
    // delete option handler  
    $('.delete_product_option').on('click', function(){
        console.log('test');
        var opt_id = $(this).attr('ref');
        console.log(opt_id);
        delete_product_option(opt_id); 
         
    });

    // submit handler
    $('#submit_edit').on('click', function(){
        console.log('test sub');
        submit_edit();
        return false;
    });
});

function add_option(name, price){
    if((typeof(name) != 'undefined') && (typeof(price) != 'undefined') && name.length> 0 && price.length>0){
        if(!$.isNumeric(price)){
            console.log('not numeric'); 
            $('#option-price').addClass('error');
            $('#option-price').val(''); 
            $('#option-price').focus();
            setTimeout(function(){ 
                $('#option-price').removeClass('error'); 
            }, 2000);
            return false;
        }
        if($('#options_table').hasClass('hide')){
            $('#options_table').removeClass('hide');
        } 
        //option_table_display = $('#options_table').css('display');
        /*if(option_table_display != 'block'){
            $('#options_table').css('display','block'); 
        } */
        
        if($('#first_option_row').length){
            var option_index = 0;    
            console.log('1a');
            var new_row = $('#first_option_row').clone();
            new_row.removeAttr('id');
            new_row.css('display','table-row');
            $('#first_option_row').remove();
        } else {
            var option_index = ($('#options_table').find('table tbody tr').length -1) + 1;
            console.log('1b');
            console.log(option_index);
            var new_row = $('#options_table tbody tr:first').clone();
        }
        new_row.find('.name').html(name);
        new_row.find('.price').html(price);
        name_input = $('<input>', { type: 'hidden', 'name': 'option-name-' + option_index }).val(name);
        price_input = $('<input>', { type: 'hidden', 'name': 'option-price-' + option_index }).val(price);
        $('#options_table tbody').append(new_row);
        $('#options_table tbody').append(name_input);
        $('#options_table tbody').append(price_input);

    } else {
        console.log('missing vals');
    }
}

function submit_edit(){
// add deleted option into form 
    // submit form
    var form = $('#edit_product_form');
    console.log(form.attr('action'));
    formData = form.serializeArray();
    console.log(formData);

    if(deleted.length>0){
        console.log('array of items to delete');
        console.log(deleted);
        for(var i in deleted){
            var opt_id = deleted[i];
            console.log(opt_id); 
            formData.push({'name':'del-opt-' + opt_id, 'value':opt_id});
        }
    }

    $.ajax({
        method: form.attr('method'), 
        url: form.attr('action'),
        data: formData,
        success: function(data){
           console.log(data); 
           $('#edit-success-alert').addClass('show');
        }
    }, "json");
}

function delete_product_option(opt_id){
    try {
        var row = $('#option_row-'+opt_id);
        row.remove();
        deleted.push(opt_id);
        // TODO: add these to the form in javascript, 
        console.log(deleted);
    } catch(e) {
        console.log(e);
    }
}
