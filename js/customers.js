$(document).ready(function(){
    $('.new_customer').on('click', function(){
        location.href = '/customer/create';
    });
    $('.edit_customer').on('click', function(){
        var product_id = $(this).attr('ref');  
        location.href = '/customer/edit/' + product_id;
    });
    $('.delete_customer').on('click', function(){
        console.log('delete customer');
        var cust_id = $(this).attr('ref'); 
        console.log(cust_id);
        $.post('/customer/delete', { customer_id: cust_id, _xsrf: $('input[name="_xsrf"]').val() }, function(data){
            console.log(data);
            var d = JSON.parse(data);
            if(d['result'] == 'ok'){
                location.href = '/customers';
            }
        });
    });
    $('.customers_csv').on('click', function(){
        console.log('download customers');
         
        $.get('/api/customers/csv', function(data){
            console.log(data);
            if(data['result'] == 'ok'){
                location.href = '/csv/customers.csv';
            }
        });
    });
});
