$(document).ready(function(){
    $('.new_transaction').on('click', function(){
        location.href = '/transaction/create';
    });
    $('.transactions_csv').on('click', function(){
        console.log('download transactions');
         
        $.get('/api/transactions/csv', function(data){
            console.log(data);
            if(data['result'] == 'ok'){
                location.href = '/csv/transactions.csv';
            }
        });
    });
});
