from functools import wraps
from digest import generate_digest
from tornado.web import HTTPError

def hmac_authorized(f):
    @wraps(f)
    def wrapper(handler, *args, **kwds):
        auth_header = handler.request.headers.get('Authorization', None)
        print("")
        print 'request'
        print handler.request
        #print 'headers'
        print handler.request.headers
        print 'auth_header'
        print auth_header
        if auth_header is not None:
            algorithm, key, digest = auth_header.split(" ")

            secret = handler.get_secret(key)
            if secret == None:
                raise HTTPError(401)

            #print "secret: %s " % secret
            #print "method: %s " % handler.request.method
            #print "path: %s " % handler.request.path
            #print "query: %s " % handler.request.query
            #print "body: %s " % handler.request.body

            expected_digest = generate_digest(
                secret, handler.request.method, handler.request.path, handler.request.query,
                handler.request.body)

            # compare_digest should be used but is not supported in python 2.7.6 - consider upgrading
            '''if not hmac.compare(digest, expected_digest): '''
            if digest != expected_digest:
                print "invalid hmac: %s" % digest
                print "expected: %s" % expected_digest 
                raise HTTPError(401)
        else:
            raise HTTPError(401)

        #print expected_digest
        return f(handler, *args, **kwds)
    return wrapper
